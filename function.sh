
VarSep(){
    IFS=',' read -a column <<< $chart_A
    #printf '%s\n' "${column[@]}"
    
    IFS=':' read -a sep <<< ${column[$1]}
    #printf '%s\n' "${sep[@]}"
    #echo ${sep[0]}

    IFS='/' read -a modul <<< ${sep[1]}
    #echo ${sep[0]}
    #printf '%s\n' "${modul[@]}"
    
    
}

grid() {

    chaineZ=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
    echo -ne '   '
	
	for ((u=0; $u<CHASSE ; u++))
	do
	    echo -ne ${chaineZ[$u]}' '
	done

	echo ' '

	for ((i=0 ; $i<$CORPS ; i++))
	do
	    if [ 10 -gt $i ]
		then echo -ne 0
	    fi

	    echo -ne $i' '
	    #echo -ne ${sep[0]}
	    for ((u=0; $u<CHASSE ; u++))
	    do
		VarSep $u
		#echo -ne ${modul[0]}
		echo -ne ${chaineZ[$u]}
		if [[ "${modul[*]}" =~ "${chaineZ[$u]}" ]];
	    then
		echo -ne █
	    else
		echo -ne ░
	    fi
	    done
	    echo ' '
	done
}
